#ifndef __DF_IPBUS_ACCESS_OK_HH__
#define __DF_IPBUS_ACCESS_OK_HH__

#include "uhal/uhal.hpp"
#include "uhal/ClientFactory.hpp"
#include <string>
#include <stdint.h>
#include <vector>
#include <map>

class df_ipbus_access_ok {
public:
  df_ipbus_access_ok(const std::string& connection_file, 
         const std::string& device_id);
  ~df_ipbus_access_ok(){};
  
private:
  uhal::ConnectionManager* m_manager;
  uhal::HwInterface*       m_hw;
};
#endif

