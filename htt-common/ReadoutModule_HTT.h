#ifndef READOUT_MODULE_HTT_H
#define READOUT_MODULE_HTT_H

#include <queue>
#include <condition_variable>
#include <future>
#include <functional>
#include <mutex>
#include <memory>
#include <functional>
#include <atomic>
#include <thread>
#include <vector>

#include "ROSCore/ReadoutModule.h"

namespace daq {
  namespace htt {

    class ReadoutModule_HTT : public ROS::ReadoutModule
    {
    public:

      // These methods are final; subclasses cannot override them.
      // The functionality should be implemented by subclasses in the pure virtual do* functions
      virtual void configure    ( const daq::rc::TransitionCmd& ) override final;
      virtual void connect      ( const daq::rc::TransitionCmd& ) override final;
      virtual void prepareForRun( const daq::rc::TransitionCmd& ) override final;
      virtual void stopROIB     ( const daq::rc::TransitionCmd& ) override final;
      virtual void stopDC       ( const daq::rc::TransitionCmd& ) override final;
      virtual void stopHLT      ( const daq::rc::TransitionCmd& ) override final;
      virtual void stopRecording( const daq::rc::TransitionCmd& ) override final;
      virtual void stopGathering( const daq::rc::TransitionCmd& ) override final;
      virtual void stopArchiving( const daq::rc::TransitionCmd& ) override final;
      virtual void disconnect   ( const daq::rc::TransitionCmd& ) override final;
      virtual void unconfigure  ( const daq::rc::TransitionCmd& ) override final;

      virtual void publish()          override final;
      virtual void publishFullStats() override final;

      // These methods are required; subclasses must implement them.
      virtual void doConfigure    ( const daq::rc::TransitionCmd& ) = 0;
      virtual void doConnect      ( const daq::rc::TransitionCmd& ) = 0;
      virtual void doPrepareForRun( const daq::rc::TransitionCmd& ) = 0;
      // (maybe we decide to rename these functions)
      virtual void doStopROIB     ( const daq::rc::TransitionCmd& ) = 0;
      virtual void doStopDC       ( const daq::rc::TransitionCmd& ) = 0;
      virtual void doStopHLT      ( const daq::rc::TransitionCmd& ) = 0;
      virtual void doStopRecording( const daq::rc::TransitionCmd& ) = 0;
      virtual void doStopGathering( const daq::rc::TransitionCmd& ) = 0;
      virtual void doStopArchiving( const daq::rc::TransitionCmd& ) = 0;
      virtual void doDisconnect   ( const daq::rc::TransitionCmd& ) = 0;
      virtual void doUnconfigure  ( const daq::rc::TransitionCmd& ) = 0;

      virtual ~ReadoutModule_HTT() noexcept;

      // disallow copy semantics
      ReadoutModule_HTT( const ReadoutModule_HTT& ) = delete;
      ReadoutModule_HTT& operator =( const ReadoutModule_HTT& ) = delete;

    protected:

      // The only constructor is protected, can't instantiate this class.
      ReadoutModule_HTT();

    private:


    };
  } 
}


#endif
